--

--[[
-- bucket of milk
minetest.register_craftitem(":mobs:bucket_milk", {
	description = S("Bucket of Milk"),
	inventory_image = "mobs_bucket_milk.png",
	stack_max = 1,
	on_use = minetest.item_eat(8, "bucket:bucket_empty"),
	groups = {food_milk = 1, flammable = 3},
})
--]]


minetest.register_craftitem("modified_rw:bucket_snow", {
	description = "Balde de Neve",
	inventory_image = "text_bucket_snow.png",
	stack_max = 1,
})

minetest.register_craft({
	output = "modified_rw:bucket_snow",
	type = "shapeless",
	recipe = {"bucket:bucket_empty", "default:snowblock"},
})

minetest.register_craft({
	type = "cooking",
	output = "bucket:bucket_river_water",
	recipe = "modified_rw:bucket_snow",
	cooktime = 10, 
	--replacements = { {"modified_rw:bucket_snow", "bucket:bucket_empty"} }
})


